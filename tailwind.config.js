import typography from '@tailwindcss/typography';
import forms from '@tailwindcss/forms';
import daisyui from 'daisyui';

import themes from 'daisyui/src/theming/themes';

/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,js,ts,svelte}'],
	darkMode: ['[data-theme="dark"]'],
	plugins: [typography, forms, daisyui],
	daisyui: {
		themes: [
			{
				light: {
					...themes['light'],
					primary: '#e65a46',
					'primary-content': '#ffffff'
				},
				dark: themes['dark']
			}
		]
	}
};
