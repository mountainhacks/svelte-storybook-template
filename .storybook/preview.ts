import type { Preview } from '@storybook/svelte';

import { withThemeByDataAttribute } from '@storybook/addon-themes';

import '../src/tailwind.css';

const preview: Preview = {
	parameters: {
		controls: {
			matchers: {
				color: /(background|color)$/i,
				date: /Date$/i
			}
		}
	}
};

export default preview;

export const decorators = [
	withThemeByDataAttribute({
		attributeName: 'data-theme',
		themes: {
			light: 'light',
			dark: 'dark'
		},
		defaultTheme: 'light'
	})
];
