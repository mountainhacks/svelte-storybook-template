import type { Meta, StoryObj } from '@storybook/svelte';

import Button from '$lib/Button.svelte';

export default {
	title: 'Design System/Atoms/Button',
	component: Button
} satisfies Meta<Button>;

type Story = StoryObj<Button>;

export const Primary: Story = {};
